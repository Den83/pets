﻿using System;

namespace Pets
{
    public abstract class Pet
    {
        public Gender Gender { get; private set; }

        public string Name { get; set; }

        public int Age { get; private set; }

        public int GrowthAtTheWithers { get; private set; }

        public Pet(Gender gender, string name, int age, int growthAtTheWithers)
        {
            this.Gender = gender;
            this.Name = name;
            this.Age = age;
            this.GrowthAtTheWithers = growthAtTheWithers;
        }

        public abstract void Voice();

        public abstract void Eat(string food, int quantity);

        public void Sleep()
        {
            Console.WriteLine("Sleeping...");
        }
    }
}

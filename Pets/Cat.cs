﻿using System;

namespace Pets
{
    public class Cat : Pet, IRun
    {
        public int ClawSize { get; set; }
        public Cat (Gender gender, string name, int age, int growthAtTheWithers, int clawSize) : base(gender, name, age, growthAtTheWithers)
        {
            this.ClawSize = clawSize;
        }

        public override void Voice()
        {
            Console.WriteLine("Meow.");
        }

        public override void Eat(string food, int quantity)
        {
            Console.WriteLine($"Cat eat {food} = {quantity}.");
        }

        public void Run()
        {
            Console.WriteLine("Cat run.");
        }

        public void Infravision()
        {
            Console.WriteLine("Cat looks with infrared vision.");
        }
    }
}


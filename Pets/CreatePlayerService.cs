﻿using System;

namespace Pets
{
    public class CreatePlayerService : ICreatePlayerService
    {
        public void Create(IPlayer player)
        {
            Player.Init(player.Gender, player.NickName, player.Birthday, player.Name);
        }
    }
}

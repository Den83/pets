﻿using System;
using System.Collections.Generic;

namespace Pets
{
    public interface ILogo
    {
        /// <summary>
        /// An interface responsible for a list, in which the number and order of the list items is read-only.
        /// </summary>
        public IReadOnlyList<string> LogoLines { get; }

        /// <summary>
        /// Logo color.
        /// </summary>
        public ConsoleColor Color { get ; }

        /// <summary>
        /// Logo background color.
        /// </summary>
        public ConsoleColor BackgroundColor { get; }
    }
}

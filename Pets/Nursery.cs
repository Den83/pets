﻿namespace Pets
{
    public class Nursery<T> : Owner<T> where T : Pet
    {
        public Nursery(string nickName) : base(nickName)
        {
        }
    }
}

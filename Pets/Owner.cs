﻿using System.Collections.Generic;

namespace Pets
{
    public abstract class Owner<T> where T : Pet
    {
        public string NickName { get; private set; }

        public List<T> Pets { get; } = new List<T>();

        public Owner(string nickName)
        {
            NickName = nickName;
        }
    }
}

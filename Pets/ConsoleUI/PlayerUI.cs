﻿using System;

namespace Pets.ConsoleUI
{
    public class PlayerUI : IPlayer
    {
        public Gender Gender { get; set; }

        public string NickName { get; set; }

        public DateTime Birthday { get; set; }

        public string Name { get; set; } 
    }
}

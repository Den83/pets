using System;
using System.Globalization;

namespace Pets.ConsoleUI
{
    public class PlayerCreator
    {
        private readonly ICreatePlayerService CreatePlayerService;

        public PlayerCreator (ICreatePlayerService сreatePlayerService)
        {
            CreatePlayerService = сreatePlayerService;
        }

        private string InputName()
        {
            Console.WriteLine("Please enter your name.");
            return Console.ReadLine();
        }

        private DateTime InputDateOfBirthday()
        {
            DateTime dateOfBirthday;
            string input;
            do
            {
                Console.WriteLine("Enter your date of birthday in format dd.MM.yyyy (day.month.year):");
                input = Console.ReadLine();
            }
            while (!DateTime.TryParseExact(input, "dd.MM.yyyy", null, DateTimeStyles.None, out dateOfBirthday));
            return dateOfBirthday;
        }

        private string InputNickName()
        {
            Console.WriteLine("Please enter your nickname.");
            return Console.ReadLine();
        }

        private Gender InputGender()
        {
            string gender;
            Gender inputGender = Gender.None;
            Console.WriteLine("Choose your gender. Type 'm' for male or 'f' for female.");
            do
            {
                gender = Console.ReadLine();
                switch (gender)
                {
                    case "m":
                        inputGender = Gender.Male;
                        break;
                    case "f":
                        inputGender = Gender.Female;
                        break;
                    default:
                        Console.WriteLine("Wrong input.");
                        break;
                } 
            }
            while (inputGender == Gender.None);
            return inputGender;
        }
        
        public void Create()
        {
            PlayerUI player = new PlayerUI();
            player.Name = InputName();
            player.NickName = InputNickName();
            player.Birthday = InputDateOfBirthday();
            player.Gender = InputGender();
            CreatePlayerService.Create(player);
        } 
    }
}

﻿using System;
using System.Linq;
using System.Threading;

namespace Pets.ConsoleUI
{
    public class Greeting
    {
        /// <summary>
        /// Displays a logo with a frame.
        /// The frame can be adjusted to the shape of the logo.
        /// </summary>
        /// <param name="x">Initial position of the cursor in the console along the X axis.</param>
        /// <param name="y">Initial position of the cursor in the console along the Y axis.</param>
        /// <param name="topMargin">Border width above the logo.</param>
        /// <param name="bottomMargin">The width of the frame below the logo.</param>
        /// <param name="leftMargin">Border width to the left of the logo.</param>
        /// <param name="rightMargin">Border width to the right of the logo.</param>
        /// <param name="logo">Game logo.</param>
        private void Print(int x, int y, int topMargin, int bottomMargin, int leftMargin, int rightMargin, Logo logo)
        {
            int nextY = y + logo.LogoLines.Count + bottomMargin;
            Console.ForegroundColor = logo.Color;
            Console.BackgroundColor = logo.BackgroundColor;
            PrintTopOrBottomMargin(x, y, topMargin, leftMargin, rightMargin, logo);
            PrintBody(x, y, leftMargin, rightMargin, topMargin, logo);
            PrintTopOrBottomMargin(x, nextY, bottomMargin, leftMargin, rightMargin, logo);
        }
        /// <summary>
        /// Prints the border above or below the logo.
        /// </summary>
        /// <param name="x">Initial position of the cursor in the console along the X axis.</param>
        /// <param name="y">Initial position of the cursor in the console along the Y axis.</param>
        /// <param name="margin">Border width above or below the logo.</param>
        /// <param name="leftMargin">Border width to the left of the logo.</param>
        /// <param name="rightMargin">Border width to the right of the logo.</param>
        /// <param name="logo">Game logo.</param>
        private void PrintTopOrBottomMargin(int x, int y, int margin, int leftMargin, int rightMargin, Logo logo)
        {
            int logoWidth = logo.LogoLines.Max().Length;
            for (int i = 0; i < margin; i++)
            {
                Console.SetCursorPosition(x, y + i);
                PrintMargin(leftMargin + rightMargin + logoWidth);
            }
        }
        /// <summary>
        /// Prints a logo with a border to the left and right.
        /// </summary>
        /// <param name="x">Initial position of the cursor in the console along the X axis.</param>
        /// <param name="y">Initial position of the cursor in the console along the Y axis.</param>
        /// <param name="leftMargin">Border width to the left of the logo.</param>
        /// <param name="rightMargin">Border width to the right of the logo.</param>
        /// <param name="topMargin">Border width above the logo.</param>
        /// <param name="logo">Game logo.</param>
        private void PrintBody(int x, int y, int leftMargin, int rightMargin, int topMargin, Logo logo)
        {
            for (int i = 0; i < logo.LogoLines.Count; i++)
            {
                int nextY = i + y + topMargin;
                Console.SetCursorPosition(x, nextY);
                PrintMargin(leftMargin);
                Console.Write(logo.LogoLines[i]);
                PrintMargin(rightMargin);
            }
        }

        private void PrintMargin(int count)
        {
            Console.Write(new string(' ', count));
        }

        private void Clear()
        {
            Console.ResetColor();
            Console.Clear();
        }

        internal void Start()
        {
            Logo logo = new Logo();
            Print(20, 5, 3, 3, 6, 6, logo);
            Thread.Sleep(1000);
            Clear();
        }
    }
}

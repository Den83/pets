﻿using System;
using System.Collections.Generic;

namespace Pets.ConsoleUI 
{
    public sealed class Logo : ILogo
    {
        private static readonly List<string> logo = new List<string>(12)
        {
            "   Greeting.   ",
            "               ",
            "               ",
            "               ",
            "      ___      ",
            "    <(-_-)>    ",
            "     ^(_)^     ",
            "     (___)     ",
            "               ",
            "               ",
            "               ",
            "Welcome to ZOO."
        };

        /// <inheritdoc />
        public ConsoleColor Color 
        { 
            get { return ConsoleColor.Red; } 
        }

        /// <inheritdoc />
        public ConsoleColor BackgroundColor
        {
            get { return ConsoleColor.DarkCyan; }
        }

        /// <inheritdoc />
        public IReadOnlyList<string> LogoLines
        {
            get { return logo.AsReadOnly(); }
        }
    }
}

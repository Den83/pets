﻿using System;
using System.Diagnostics;

namespace Pets.ConsoleUI
{
    internal class MenuUI
    {
        /// <summary>
        /// Start menu.
        /// </summary>
        internal void StartMenu()
        {
            bool reboot = true;
            while (reboot)
            {
                Console.Clear();
                Console.WriteLine("### MENU ### \n1. Play.\n2. About.\n3. Authors.\n4.  Exit.\nEnter command: ");
                string selection = WaitInput("1", "2", "3", "4");
                Console.Clear();
                switch (selection)
                {
                    case "1":
                        Console.Clear();
                        ICreatePlayerService createPlayerService = new CreatePlayerService();
                        PlayerCreator playerCreator = new PlayerCreator(createPlayerService);
                        playerCreator.Create();
                        reboot = false;
                        break;
                    case "2":
                        Console.WriteLine("\n\nWelcome, player.Create your own zoo and become its virtual administrator." +
                            "\nPlay, buy, feed the animals in your zoo.\n\nPress 5 to return to the menu.");
                        WaitInput("5");
                        break;
                    case "3":
                        Console.WriteLine("\n\nGame developer : Vladimir Danilov.\n\nPress 5 to return to the menu.", "5");
                        WaitInput("5");
                        break;
                    case "4":
                        Environment.Exit(0);
                        break;
                }
            }
        }

        /// <summary>
        /// Waiting for one of the valid values ​​to be entered.
        /// </summary>
        /// <param name="possibleValues">Possible values ​​on input.</param>
        /// <returns>The entered value.</returns>
        private string WaitInput(params string[] possibleValues)
        {
            Debug.Assert(possibleValues != null && possibleValues.Length != 0, "possibleValues is empty");
            string values = string.Join(",", possibleValues);
            string input;
            while (Array.IndexOf(possibleValues, input = Console.ReadLine()) == -1)
                Console.WriteLine($"Wrong input. Possible values: {values}.");
            return input;
        }
    }
}
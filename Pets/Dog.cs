﻿using System;

namespace Pets
{
    public class Dog : Pet, IRun
    {
        public string Breed { get; private set; }

        public Dog(Gender gender, string name, int age, int growthAtTheWithers, string breed) : base(gender, name, age, growthAtTheWithers)
        {
            Breed = breed;
        }

        public override void Voice()
        {
            Console.WriteLine("Woof.");
        }

        public override void Eat(string food, int quantity)
        {
            Console.WriteLine($"Dog eat {food} = {quantity}.");
        }

        public void Run()
        {
            Console.WriteLine("Dog run.");
        }

        public void Bites()
        {
            Console.WriteLine("Dog bites.");
        }

        public void Growl()
        {
            Console.WriteLine("blabla");
        }

        
    }
}

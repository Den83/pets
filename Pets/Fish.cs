﻿using System;

namespace Pets
{
    public class Fish : Pet
    {
        public string Scales { get; set; }

        public Fish(Gender gender, string name, int age, int growthAtTheWithers, string scales) : base(gender, name, age, growthAtTheWithers)
        {
            Scales = scales;
        }

        public override void Voice()
        {
            Console.WriteLine("Bul_bul.");
        }

        public override void Eat(string food, int quantity)
        {
            Console.WriteLine($"Fish eat {food} = {quantity}.");
        }

        public void Swim()
        {
            Console.WriteLine("Swimming...");
        }
    }
}

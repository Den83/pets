﻿using System;

namespace Pets
{
    public interface IPlayer
    {
        Gender Gender { get; }

        string NickName { get; }

        DateTime Birthday { get; }

        string Name { get; }
    }
}

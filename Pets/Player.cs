﻿using System;

namespace Pets
{
    public class Player : Person
    {
        public string Name { get; }

        public DateTime Birthday { get; }

        public static Player Instance { get; private set; }

        private Player(Gender gender, string nickName, DateTime birthday, string name) : base(gender, nickName)
        {
            Name = name;
            Birthday = birthday;
        }

        public static void Init(Gender gender, string nickName, DateTime birthday, string name)
        {
            if (Instance == null)
                Instance = new Player(gender, nickName, birthday, name);
            else throw new InvalidOperationException("The player already has been created.");
        }

    }
}

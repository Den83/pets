﻿namespace Pets
{
    public interface ICreatePlayerService
    {
       void Create(IPlayer player);
    }
}

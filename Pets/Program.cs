﻿using Pets.ConsoleUI;
using System;

namespace Pets
{
    class Program
    {
       
        static void Main(string[] args) {
            Greeting logo = new Greeting();
            logo.Start();
            MenuUI menu = new MenuUI();
            menu.StartMenu();
        }
    }
}


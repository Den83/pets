﻿using System;

namespace Pets
{
    public class Person: Owner<Pet>
    {
        public Gender Gender { get; private set; }

        public Person(Gender gender, string nickName) : base(nickName)
        {
            Gender = gender;
        }

        public void AddPet(Pet pet)
        {
            Pets.Add(pet);
            Console.WriteLine($"The owner {NickName} got new pet {pet.Name}.");
        }

        public void FeedThePet(Pet pet, string food, int quantity)
        {
            Console.WriteLine($"The owner {NickName} feeds the {pet.Name}. {pet.Name} eat {food} = {quantity}.");
        }

        public void Play(Pet pet)
        {
            Console.WriteLine($"The owner {NickName} plays with the {pet.Name}.");
        }

        public void Run(Pet pet)
        {
            IRun runnable = (pet as IRun);
            if (runnable != null)
            {
                runnable.Run();
                Console.WriteLine($"The owner {NickName} walks with the {pet.Name}.");
            }
        }

        public void WalkWithPets()
        {
            foreach (Pet pet in Pets)
            {
                Run(pet);
            }
        }

        public void ChangeWater()
        {
            foreach (Pet pet in Pets)
            {
                if (pet is Fish)
                    Console.WriteLine($"The owner {NickName} changes the water in the fish {pet.Name}.");
            }
        }
    }
}

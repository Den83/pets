﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pets.ConsoleUI;
using Pets.Tests.Components;
using System;
using System.IO;

namespace Pets.Tests
{
    /// <summary>
    /// For testing <see cref="PlayerCreator"/>.
    /// </summary>
    [TestClass]
    public class TestPlayerCreator
    {
        /// <summary>
        /// Player creation check.
        /// </summary>
        [TestMethod]
        public void TestCreatePlayer()
        {
            // Actual data
            const string actualName = "TestName";
            const string actualNickName = "TestNickName";
            const string actualBirthday = "01.01.2000";
            const string genderInput = "m";
            const Gender actualGender = Gender.Male;

            StringWriter output = new StringWriter();
            Console.SetOut(output);

            StringReader input = new StringReader($"{actualName}\n{actualNickName}\n{actualBirthday}\n{genderInput}");
            Console.SetIn(input);
            
            TestCreatePlayerService createPlayerService = new TestCreatePlayerService();
            PlayerCreator playerCreator = new PlayerCreator(createPlayerService);

            // Act.
            playerCreator.Create();
            IPlayer player = createPlayerService.Player;

            // Assert.

            Assert.AreEqual(output.ToString(), string.Format("Please enter your name.{0}Please enter your nickname." +
                "{0}Enter your date of birthday in format dd.MM.yyyy (day.month.year):" +
                "{0}Choose your gender. Type 'm' for male or 'f' for female.{0}", Environment.NewLine));

            Assert.AreEqual(player.Name, actualName);
            Assert.AreEqual(player.NickName, actualNickName);
            Assert.AreEqual(player.Birthday.ToString("dd.MM.yyyy"), actualBirthday);
            Assert.AreEqual(player.Gender, actualGender);
        }
    }
}

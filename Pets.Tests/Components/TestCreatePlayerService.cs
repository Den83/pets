﻿namespace Pets.Tests.Components
{
    internal class TestCreatePlayerService : ICreatePlayerService
    {
        public IPlayer Player { get; private set; }

        public void Create(IPlayer player)
        {
            Player = player;
        }
    }
}
